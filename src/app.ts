import { Driver as Chrome } from 'selenium-webdriver/chrome'
import { WebDriver, Capabilities, Builder } from 'selenium-webdriver'
import axios from 'axios';


async function getLatestChromeDriverVersion(): Promise<void> {
    axios.get('https://chromedriver.storage.googleapis.com/LATEST_RELEASE')
    .then(response => console.log(response.data))
    .catch(error => console.log(error));
}

async function startSession() {
    let driver = await new Builder().forBrowser('chrome').build();
    await driver.get('https://code.pieces.app/getting-started/chrome');
};

getLatestChromeDriverVersion();